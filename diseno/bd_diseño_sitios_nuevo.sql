CREATE TABLE IF NOT EXISTS `software_claro`.`cl_historial` (
  `cl_histo_id` VARCHAR(40) NOT NULL,
  `cl_histo_destinatario` VARCHAR(100) NULL,
  `cl_histo_cc` VARCHAR(100) NULL,
  `cl_histo_asunto` VARCHAR(100) NULL,
  `cl_histo_ip` VARCHAR(50) NULL,
  `cl_histo_usuario` VARCHAR(45) NULL,
  `cl_histo_fecha` DATETIME NULL,
  PRIMARY KEY (`cl_histo_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `software_claro`.`cl_historial_det` (
  `cl_histo_det_id` INT NOT NULL,
  `cl_histo_id_sitio` VARCHAR(45) NULL,
  `cl_histo_id_sitio_nombre` VARCHAR(100) NULL,
  `cl_histo_latitud` VARCHAR(45) NULL,
  `cl_histo_longitud` VARCHAR(45) NULL,
  `cl_histo_mnemonico` VARCHAR(100) NULL,
  `cl_histo_municipio` VARCHAR(100) NULL,
  `cl_histo_departamento` VARCHAR(100) NULL,
  `cl_histo_direccion` VARCHAR(100) NULL,
  `cl_histo_descripcion` VARCHAR(100) NULL,
  `cl_histo_descripcion_arbol_carpeta` VARCHAR(150) NULL,
  `cl_historial_cl_histo_id` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`cl_histo_det_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `software_claro`.`cl_usuarios` (
  `cl_usu_id` INT NOT NULL,
  `cl_usu_nombre` VARCHAR(100) NULL,
  `cl_usu_usuario` VARCHAR(100) NULL,
  `cl_usu_password` VARCHAR(100) NULL,
  `cl_usu_departamento` VARCHAR(45) NULL,
  `cl_usu_ip` VARCHAR(45) NULL,
  `cl_usu_fecha` DATETIME NULL,
  PRIMARY KEY (`cl_usu_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `software_claro`.`cl_basegeneral` (
  `id_contador` VARCHAR(100) NOT NULL,
  `operador` VARCHAR(45) NULL,
  `sitio` VARCHAR(100) NULL,
  `site_name` VARCHAR(150) NULL,
  `id_claro` VARCHAR(45) NULL,
  `departamento` VARCHAR(100) NULL,
  `municipio` VARCHAR(100) NULL,
  `ing_cell_type` VARCHAR(100) NULL,
  `altura_de_rf` VARCHAR(100) NULL,
  `latitud` VARCHAR(100) NULL,
  `longitud` VARCHAR(100) NULL,
  `medio_de_tx` VARCHAR(45) NULL,
  `estructura_sitio` VARCHAR(45) NULL,
  `infra` VARCHAR(45) NULL,
  `topologia` VARCHAR(100) NULL,
  `terreno_arrendador` VARCHAR(100) NULL,
  `terreno_renta_mensual` VARCHAR(100) NULL,
  `terreno_vigencia_contrato` VARCHAR(100) NULL,
  `terreno_periodo_forzoso` VARCHAR(100) NULL,
  `terreno_arrendador` VARCHAR(100) NULL,
  `terreno_renta_mensual` VARCHAR(100) NULL,
  `terreno_vigencia_contrato` VARCHAR(100) NULL,
  `terreno_periodo_forzoso` VARCHAR(100) NULL,
  `condiciones_operativos_torre` VARCHAR(100) NULL,
  `coubicacion_de_terceros` VARCHAR(100) NULL,
  `restriccion_de_acceso` VARCHAR(100) NULL,
  `licencias_vigentes` VARCHAR(100) NULL,
  `altura_de_torre` VARCHAR(100) NULL,
  `marca_de_la_torre` VARCHAR(100) NULL,
  `velocidad_torre` VARCHAR(100) NULL,
  `antiguedad_de_la_torre` VARCHAR(100) NULL,
  `tipo_estructura` VARCHAR(100) NULL,
  `ultimo_manteniminento` VARCHAR(100) NULL,
  `planos_de_la_torre` VARCHAR(100) NULL,
  `Planos_del_sitio` VARCHAR(100) NULL,
  `tipo_de_energia` VARCHAR(100) NULL,
  `marn` VARCHAR(100) NULL,
  `dgac` VARCHAR(100) NULL,
  `licencia_construccion` VARCHAR(100) NULL,
  `cocode` VARCHAR(100) NULL,
  `otros_aportes` VARCHAR(100) NULL,
  `Problemas_sociales_muni` VARCHAR(100) NULL,
  `posibles_penalizaciones` VARCHAR(100) NULL,
  `torre_identificada` VARCHAR(100) NULL,
  `equipos_radio_enlace` VARCHAR(100) NULL,
  PRIMARY KEY (`id_contador`))
ENGINE = InnoDB;

use software_claro;

select * from cl_usuarios;
select cl_usu_nombre 
from cl_usuarios 
where cl_usu_password ='admin123' and cl_usu_usuario='admin';



insert into cl_usuarios (
cl_usu_id,
cl_usu_nombre,
cl_usu_usuario,
cl_usu_password,
cl_usu_departamento,
cl_usu_ip,
cl_usu_fecha
)values(
'2',
'Diego Mijangos',
'admindos',
'admin1234',
'obra civil',
'192.168.0.1',
STR_TO_DATE('08-10-2019', '%d-%m-%Y'));

select * from cl_historial


insert into cl_historial (
	cl_histo_id,
    cl_histo_destinatario,
    cl_histo_cc,
    cl_histo_asunto,
    cl_histo_ip,
    cl_histo_usuario,
    cl_histo_fecha,
)values(
	'1',
    'prueba@prueba.com',
    'destino@prueba.com',
    'prueba',
    '192.168.1.1',
    'admin',
    STR_TO_DATE('08-10-2019', '%d-%m-%Y'));








