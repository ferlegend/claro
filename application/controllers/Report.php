<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends CI_Controller {

  // construct
     public function __construct() {
         parent::__construct();
         // load model
         $this->load->model('reportes');
     }

	public function index()
	{
    $this->load->library('session');
    $rol = $_SESSION["role"];
    $this->load->helper('form');
        $this->load->helper('url');
        switch ($rol) {
          case '1':
            // code...
              redirect('restrinct');
            break;
          case '2':
              redirect('restrinct');
           break;
          case '3':
              redirect('restrinct');
            break;
          case '4':
                redirect('restrinct');
              break;
          case '5':
            // code...
            //$this->load->view('dashboard');
          //	$this->load->view('accessdenied.php');
          redirect('restrinct');
            //redirect('restrinct');
            break;
          default:
           $redirect = base_url()."/index.php/welcome/login";
            // code...
            redirect('/login');
            break;

        }

	}
  public function arealegal(){
    // create file name

    // en los excel el campo de  categoria es el campo de operador
    $this->load->library('session');
    $rol = $_SESSION["role"];
    $this->load->helper('form');
        $this->load->helper('url');

    switch ($rol) {
      case '1':
        // code...
        $fileName = 'data-'.time().'.xlsx';
        // load excel library
        $this->load->library('excel');
        $listInfo = $this->reportes->area_legal();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id_claro');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'site_name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'departamento');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'municipio');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'latitud');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'longitud');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ing_cell_type');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'estructura_de_sitio');
         $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'licencia_de_construccion');
        // set Row
        $rowCount = 2;
        foreach ($listInfo as $list) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->id_claro);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->site_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->departamento);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->municipio);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->latitud);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->longitud);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->ing_cell_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->estructura_de_sitio);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $list->licencia_construccion);
            $rowCount++;
        }
        $filename = "Reporte_area_legal_licencias_construccion"."-".date("d-m-Y").".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');



        break;
      case '2':
      $fileName = 'data-'.time().'.xlsx';
      // load excel library
      $this->load->library('excel');
      $listInfo = $this->reportes->area_legal();
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->setActiveSheetIndex(0);
      // set Header
      $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id_claro');
      $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'site_name');
      $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'departamento');
      $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'municipio');
      $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'latitud');
      $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'longitud');
      $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ing_cell_type');
      $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'estructura_de_sitio');
       $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'licencia_de_construccion');
      // set Row
      $rowCount = 2;
      foreach ($listInfo as $list) {
          $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->id_claro);
          $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->site_name);
          $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->departamento);
          $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->municipio);
          $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->latitud);
          $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->longitud);
          $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->ing_cell_type);
          $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->estructura_de_sitio);
          $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $list->licencia_construccion);
          $rowCount++;
      }
      $filename = "Reporte_area_legal_licencias_construccion"."-".date("d-m-Y").".csv";
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$filename.'"');
      header('Cache-Control: max-age=0');
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
      $objWriter->save('php://output');


        // code...
        //$this->load->view('dashboard');
        break;
      case '3':
      $fileName = 'data-'.time().'.xlsx';
      // load excel library
      $this->load->library('excel');
      $listInfo = $this->reportes->area_legal();
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->setActiveSheetIndex(0);
      // set Header
      $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id_claro');
      $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'site_name');
      $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'departamento');
      $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'municipio');
      $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'latitud');
      $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'longitud');
      $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ing_cell_type');
      $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'estructura_de_sitio');
       $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'licencia_de_construccion');
      // set Row
      $rowCount = 2;
      foreach ($listInfo as $list) {
          $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->id_claro);
          $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->site_name);
          $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->departamento);
          $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->municipio);
          $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->latitud);
          $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->longitud);
          $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->ing_cell_type);
          $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->estructura_de_sitio);
          $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $list->licencia_construccion);
          $rowCount++;
      }
      $filename = "Reporte_area_legal_licencias_construccion"."-".date("d-m-Y").".csv";
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$filename.'"');
      header('Cache-Control: max-age=0');
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
      $objWriter->save('php://output');


        // code...
        //$this->load->view('dashboard');
        break;
      case '4':
        // code...
        //$this->load->view('dashboard');
      //	$this->load->view('accessdenied.php');
        //redirect('restrinct');
        $fileName = 'data-'.time().'.xlsx';
        // load excel library
        $this->load->library('excel');
        $listInfo = $this->reportes->area_legal();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id_claro');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'site_name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'departamento');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'municipio');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'latitud');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'longitud');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ing_cell_type');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'estructura_de_sitio');
         $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'licencia_de_construccion');
        // set Row
        $rowCount = 2;
        foreach ($listInfo as $list) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->id_claro);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->site_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->departamento);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->municipio);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->latitud);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->longitud);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->ing_cell_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->estructura_de_sitio);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $list->licencia_construccion);
            $rowCount++;
        }
        $filename = "Reporte_area_legal_licencias_construccion"."-".date("d-m-Y").".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');






        break;
        case '5':
          // code...
          //$this->load->view('dashboard');
        //	$this->load->view('accessdenied.php');
        redirect('restrinct');
          //redirect('restrinct');
          break;
      default:
      redirect('restrinct');
        // code...
        break;
    }



  }
  public function permisos_municipales(){


    $this->load->library('session');
    $rol= $_SESSION["role"];

    switch ($rol) {
      case '1':
        // code...
        // create file name
           $fileName = 'data-'.time().'.xlsx';
           // load excel library
           $this->load->library('excel');
           $listInfo = $this->reportes->area_legal_permisos_municipales();
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           // set Header
           $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id_claro');
           $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'site_name');
           $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'departamento');
           $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'municipio');
           $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'latitud');
           $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'longitud');
           $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ing_cell_type');
           $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'estructura_de_sitio');
           // set Row
           $rowCount = 2;
           foreach ($listInfo as $list) {
               $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->id_claro);
               $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->site_name);
               $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->departamento);
               $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->municipio);
               $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->latitud);
               $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->longitud);
               $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->ing_cell_type);
               $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->estructura_de_sitio);
               $rowCount++;
           }
           $filename = "Reporte_area_legal_permisos_municipales"."-".date("d-m-Y").".csv";
           header('Content-Type: application/vnd.ms-excel');
           header('Content-Disposition: attachment;filename="'.$filename.'"');
           header('Cache-Control: max-age=0');
           $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
           $objWriter->save('php://output');






        break;
      case '2':
      $fileName = 'data-'.time().'.xlsx';
      // load excel library
      $this->load->library('excel');
      $listInfo = $this->reportes->area_legal_permisos_municipales();
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->setActiveSheetIndex(0);
      // set Header
      $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id_claro');
      $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'site_name');
      $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'departamento');
      $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'municipio');
      $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'latitud');
      $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'longitud');
      $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ing_cell_type');
      $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'estructura_de_sitio');
      // set Row
      $rowCount = 2;
      foreach ($listInfo as $list) {
          $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->id_claro);
          $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->site_name);
          $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->departamento);
          $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->municipio);
          $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->latitud);
          $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->longitud);
          $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->ing_cell_type);
          $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->estructura_de_sitio);
          $rowCount++;
      }
      $filename = "Reporte_area_legal_permisos_municipales"."-".date("d-m-Y").".csv";
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$filename.'"');
      header('Cache-Control: max-age=0');
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
      $objWriter->save('php://output');
        // code...
        //$this->load->view('dashboard');
        break;
      case '3':
        // code...
        //$this->load->view('dashboard');
        $fileName = 'data-'.time().'.xlsx';
        // load excel library
        $this->load->library('excel');
        $listInfo = $this->reportes->area_legal_permisos_municipales();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id_claro');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'site_name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'departamento');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'municipio');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'latitud');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'longitud');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ing_cell_type');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'estructura_de_sitio');
        // set Row
        $rowCount = 2;
        foreach ($listInfo as $list) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->id_claro);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->site_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->departamento);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->municipio);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->latitud);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->longitud);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->ing_cell_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->estructura_de_sitio);
            $rowCount++;
        }
        $filename = "Reporte_area_legal_permisos_municipales"."-".date("d-m-Y").".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');

        break;
      case '4':
        // code...
        //$this->load->view('dashboard');
      //	$this->load->view('accessdenied.php');
        //redirect('restrinct');
        $fileName = 'data-'.time().'.xlsx';
        // load excel library
        $this->load->library('excel');
        $listInfo = $this->reportes->area_legal_permisos_municipales();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id_claro');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'site_name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'departamento');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'municipio');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'latitud');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'longitud');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ing_cell_type');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'estructura_de_sitio');
        // set Row
        $rowCount = 2;
        foreach ($listInfo as $list) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->id_claro);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->site_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->departamento);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->municipio);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->latitud);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->longitud);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->ing_cell_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->estructura_de_sitio);
            $rowCount++;
        }
        $filename = "Reporte_area_legal_permisos_municipales"."-".date("d-m-Y").".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');

        break;

      default:
      case '5':
        // code...
        //$this->load->view('dashboard');
      //	$this->load->view('accessdenied.php');
        redirect('restrinct');
        break;
      redirect('restrinct');
        // code...
        break;
    }






  }
}
