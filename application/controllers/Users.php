<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function index()
	{

		$this->load->helper('url');
			$this->load->library('session');
		$this->load->model('model_user');

		$rol= $_SESSION["role"];
		switch ($rol) {
			case '1':
				// code...
				$data["usuarios"]= $this->model_user->user();
				$this->load->view('users',$data);
				break;
			case '2':
				// code...
				redirect('restrinct');
				break;
			case '3':
			$data["usuarios"]= $this->model_user->user();
			$this->load->view('subgerente/users',$data);
				// code...
				//$this->load->view('dashboard');
				break;
			case '4':
				// code...
				//$this->load->view('dashboard');
			//	$this->load->view('accessdenied.php');
				redirect('restrinct');
				break;
				case '5':
					// code...
					//$this->load->view('dashboard');
				//	$this->load->view('accessdenied.php');
					redirect('restrinct');
					break;

			default:
			redirect('restrinct');
				// code...
				break;
		}
		//var_dump($usuarios);


	}

	public function ip()
	{
    // funcion para capturar la ip del visitante y guardarla
    if (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
      } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
      } elseif (getenv('HTTP_X_FORWARDED')) {
        $ip = getenv('HTTP_X_FORWARDED');
      } elseif (getenv('HTTP_FORWARDED_FOR')) {
        $ip = getenv('HTTP_FORWARDED_FOR');
      } elseif (getenv('HTTP_FORWARDED')) {
        $ip = getenv('HTTP_FORWARDED');
      } else {
    // Método por defecto de obtener la IP del usuario
    // Si se utiliza un proxy, esto nos daría la IP del proxy
    // y no la IP real del usuario.
        $ip = $_SERVER['REMOTE_ADDR'];
        }
        //echo "Su IP parece ser: ".$ip;
				return $ip;
	}
	public function nuevo(){

		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('model_user');
		$rol= $_SESSION["role"];
		switch ($rol) {
			case '1':
				// code...

				$data["tipouser"]= $this->model_user->tipo_de_usuarios();
				//var_dump($data["tipouser"]);

				$data["response"]=trim(isset($_REQUEST["response"]));
				$this->load->view('nuevo_users',$data);
				break;
			case '2':
				// code...
				redirect('restrinct');
				break;
			case '3':

			$data["tipouser"]= $this->model_user->tipo_de_usuarios();
			//var_dump($data["tipouser"]);

			$data["response"]=trim(isset($_REQUEST["response"]));
			$this->load->view('subgerente/nuevo_users',$data);

				// code...
				//$this->load->view('dashboard');
				break;
			case '4':
				// code...
				//$this->load->view('dashboard');
			//	$this->load->view('accessdenied.php');
				redirect('restrinct');
				break;
				case '5':
					// code...
					//$this->load->view('dashboard');
				//	$this->load->view('accessdenied.php');
					redirect('restrinct');
					break;

			default:
			redirect('restrinct');
				// code...
				break;
		}






	}
	public function guardar(){
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('model_user');

	//	$id = trim($_REQUEST["id"]);
		$nombre= trim($_REQUEST["nombre"])." ".trim($_REQUEST["apellido"]);
		//$apellido=trim($_REQUEST["apellido"]);
		$usuario = trim($_REQUEST["username"]);
		$password = trim($_REQUEST["password"]);
		$correo = trim($_REQUEST["email"]);
		$ip= $this->ip();
		$fecha=date('d-m-Y');
		$tipo=trim($_REQUEST['tipo']);
		//$this->load->model('model_user');
		if (empty($usuario)) {
			header("Location: http://192.168.0.5:8888/claro/index.php/users/nuevo/");
			die();

		}else{
			$data["guardar"] = $this->model_user->guardar($nombre,$usuario,$password,$ip,$fecha,$correo,$tipo);
			header("Location: http://192.168.0.5:8888/claro/index.php/users/nuevo/?response=1");
			die();

		}
	}
	public function delete(){
			$this->load->helper('url');
			$this->load->library('session');
			$this->load->model('model_user');
			//$data["boraruser"] = $this->model_user->delete();
				if(isset($_REQUEST["id"])) {
					// code...
					$id = $_REQUEST["id"];
					$data["boraruser"] = $this->model_user->delete($id);

					header("Location: http://192.168.0.5:8888/claro/index.php/users");
					die();
				}else {

					header("Location: http://192.168.0.5:8888/claro/index.php/welcome/login");
					die();
				}
	}
	public function editar(){
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('model_user');
		$this->load->model('model_typeuser');
		$this->load->helper('form');

		$rol= $_SESSION["role"];
		switch ($rol) {
			case '1':
				// code...
				$data["tipousuario"]= $this->model_typeuser->tipos_usuarios();

				$data['id']=trim($_REQUEST["id"]);
				$data["nombre"]=trim($_REQUEST["nombre"]);
				$data["usuario"]=trim($_REQUEST["usuario"]);
				$this->load->view('editar_usuario',$data);




				break;
			case '2':
				// code...
				redirect('restrinct');
				break;
			case '3':
			$data["tipousuario"]= $this->model_typeuser->tipos_usuarios();

			$data['id']=trim($_REQUEST["id"]);
			$data["nombre"]=trim($_REQUEST["nombre"]);
			$data["usuario"]=trim($_REQUEST["usuario"]);
			$this->load->view('subgerente/editar_usuario',$data);




				// code...
				//$this->load->view('dashboard');
				break;
			case '4':
				// code...
				//$this->load->view('dashboard');
			//	$this->load->view('accessdenied.php');
				redirect('restrinct');
				break;
				case '5':
					// code...
					//$this->load->view('dashboard');
				//	$this->load->view('accessdenied.php');
					redirect('restrinct');
					break;

			default:
			redirect('restrinct');
				// code...
				break;
		}












//var_dump($data["tipousuario"]);
	}
	public function userupdate(){
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('model_user');


		$data['id']=trim($_REQUEST["id"]);
		$data["nombre"]=trim($_REQUEST["nombre"]);
		$data["usuario"]=trim($_REQUEST["username"]);
		$tipo = trim($_REQUEST["tipousuario"]);
		$nuevocorreo= trim($_REQUEST["nuevocorreo"]);

		echo $data['id']."<br>";
		echo $data['nombre']."<br>";
		echo $data['usuario']."<br>";
		echo $tipo;

		$data['actualizados']=$this->model_user->actualizar($data["nombre"],$data["usuario"],	$data['id'],$tipo,$nuevocorreo);
		header("Location: http://192.168.0.5:8888/claro/index.php/users");
		die();
	//	if (!is_null($data["id"])) {
			// code...
			//$data['actualizados']=$this->model_user->actualizar($data["nombre"],$data["usuario"],	$data['id'],$tipo);
		//	header("Location: http://192.168.0.5:8888/claro/index.php/users");
			//die();
	//	}


	}


	public function renewpassword(){
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('model_user');

		$this->load->view('confirmacion_correo_usuario');


	}

	public function  send(){
			/* ================================================= */

			$config = array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://gator4167.hostgator.com',
					'smtp_port' => 465,
					'smtp_user' => 'noreply@ltechweb.org',
					'smtp_pass' => 'xxxxxx',
					'mailtype' => 'html',
					'charset' => 'iso-8859-1',
					'newline' => '\r\n',
					'crlf' => '\r\n',
			);



			//$mensaje= $this->load->view('plantilla html');

			$this->load->library('email',$config);
			$this->load->library('parser');
			$correo='dmijangos@ltechweb.org';
			$copia='lmijangos@ltechweb.org';
			$this->email->from('hack@hack.com', 'Tu cuenta ha sido hackeada.');
			$this->email->to($correo);
			$this->email->cc($copia);
			$this->email->subject('Datos personales');
			$this->email->message('mensaje');
			$this->email->send();

			//$this->load->mail();

	}
	public function typeusers(){

			$this->load->view('typeusers');


	}
	public function admin(){
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->view('admin/users');

	}




}
