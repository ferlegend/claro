<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Importar extends CI_Controller {
  	public function __construct(){
  		parent::__construct();
  		// load base_url
  		$this->load->helper('url');
  		// Load Model
  		$this->load->model('Main_model');
  	}

  	public function index(){
  		// Check form submit or not
      $this->load->library('session');
      $rol= $_SESSION["role"];

      echo $rol;
      switch ($rol) {
				case '1':
					// code...
					//$this->load->view('dashboard');
            // inicia la funcion de importar
            if($this->input->post('upload') != NULL ){
                $data = array();
                if(!empty($_FILES['file']['name'])){
                  // Set preference
                  $config['upload_path'] = 'assets/files/';
                  $config['allowed_types'] = 'csv';
                  $config['max_size'] = '1000'; // max_size in kb
                  $config['file_name'] = $_FILES['file']['name'];
                  // Load upload library
                  $this->load->library('upload',$config);
                  // File upload
                  if($this->upload->do_upload('file')){
                    // Get data about the file
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    // Reading file
                            $file = fopen("assets/files/".$filename,"r");
                            $i = 0;
                            $importData_arr = array();
                            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                                $num = count($filedata);
                                for ($c=0; $c < $num; $c++) {
                                    $importData_arr[$i][] = $filedata[$c];
                                }
                                $i++;
                            }
                            fclose($file);
                            $skip = 0;
                            // insert import data
                            foreach($importData_arr as $userdata){
                                if($skip != 0){
                                    $this->Main_model->insertRecord($userdata);
                                }
                                $skip ++;
                            }
                    $data['response'] = 'El archivo ha sido subido exitosamente.  '.$filename;
                  }else{
                    $data['response'] = 'Error no se pudo actualizar datos';
                  }
                }else{
                  $data['response'] = 'Error no se pudo actualizar datos';
                }
                // load view
                $this->load->view('users_view',$data);
              }else{
                // load view
                $this->load->view('users_view');
              }


            // finaliza la funcion de importar
					break;
				case '2':
        if($this->input->post('upload') != NULL ){
            $data = array();
            if(!empty($_FILES['file']['name'])){
              // Set preference
              $config['upload_path'] = 'assets/files/';
              $config['allowed_types'] = 'csv';
              $config['max_size'] = '1000'; // max_size in kb
              $config['file_name'] = $_FILES['file']['name'];
              // Load upload library
              $this->load->library('upload',$config);
              // File upload
              if($this->upload->do_upload('file')){
                // Get data about the file
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                // Reading file
                        $file = fopen("assets/files/".$filename,"r");
                        $i = 0;
                        $importData_arr = array();
                        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                            $num = count($filedata);
                            for ($c=0; $c < $num; $c++) {
                                $importData_arr[$i][] = $filedata[$c];
                            }
                            $i++;
                        }
                        fclose($file);
                        $skip = 0;
                        // insert import data
                        foreach($importData_arr as $userdata){
                            if($skip != 0){
                                $this->Main_model->insertRecord($userdata);
                            }
                            $skip ++;
                        }
                $data['response'] = 'El archivo ha sido subido exitosamente. '.$filename;
              }else{
                $data['response'] = 'Error no se pudo actualizar datos';
              }
            }else{
              $data['response'] = 'Error no se pudo actualizar datos';
            }
            // load view
            $this->load->view('users_view',$data);
          }else{
            // load view
            $this->load->view('users_view');
          }

					// code...
					//$this->load->view('dashboard');
					break;
				case '3':
					// code...
					//$this->load->view('dashboard');
          redirect('restrinct');
					break;
				case '4':
					// code...
					//$this->load->view('dashboard');
				//	$this->load->view('accessdenied.php');
					redirect('restrinct');
					break;

				default:
        case '5':
          // code...
          //$this->load->view('dashboard');
        //	$this->load->view('accessdenied.php');
          redirect('restrinct');
          break;
				redirect('restrinct');
					// code...
					break;
			}



      //iniciaba el if






    // cierra la funcion
  	}






  }
