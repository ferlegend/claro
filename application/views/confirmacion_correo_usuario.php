<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> Dashboard</title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/img/preview-Claro.ico">
        <!--STYLESHEET-->
        <!--=================================================-->
        <!--Roboto Slab Font [ OPTIONAL ] -->
        <!--Bootstrap Stylesheet [ REQUIRED ]-->

        <?php require 'css.php'; ?>
        <!--Summernote [ OPTIONAL ]-->

</script>

    </head>
    <body>
        <div id="container" class="effect mainnav-sm navbar-fixed mainnav-fixed">
            <!--NAVBAR-->
            <!--===================================================-->
            <header id="navbar">
                <div id="navbar-container" class="boxed">
                    <!--Navbar Dropdown-->
                    <!--================================-->

                      <nav class="navbar navbar-dark bg-danger" style="background-color: #c41404;">
                        <span class="navbar-brand"><i><img src="<?php echo base_url(); ?>/assets/img/SCL.png" width="40"> <font size="5" face="georgia">Obra Civil</font></i></span>
                      </nav>



                    <!--================================-->
                    <!--End Navbar Dropdown-->
                </div>
            </header>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">

                    <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                      <br><br>
                        <div class="row">
                          <div class="col-md-3">


                          </div>
                          <div class="col-md-6">
                              <div class="card">
                                  <h5 class="card-header">Restablecimiento de contraseña</h5>

                                  <div class="card-body">
                                      <p class="card-text">Ingrese su Usuario</p>
                                      <form class="" action="index.html" method="post">
                                        <input type="text" name="Usuario" value="">
                                        <button type="button" class="btn btn-danger" value="<?= echo $codigo; ?>">Validar</button>
                                      </form>
                                      <p class="card-text">revise su correo  se enviara un codigo el cual debera ingresarlo para validación.</p>
                                  </div>
                                  <div class="card-footer">

                                  </div>
                              </div>
                          </div>
                          <div class="col-md-3">

                          </div>
                        </div>




                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->


                <!--===================================================-->
                <!--END MAIN NAVIGATION-->
            </div>
            <!-- FOOTER -->
            <!--===================================================-->

            <!--===================================================-->
            <!-- END FOOTER -->
            <!-- SCROLL TOP BUTTON -->
            <!--===================================================-->

            <!--===================================================-->
        </div>
        <!--===================================================-->
        <!-- END OF CONTAINER -->
        <!--JAVASCRIPT-->
        <!--=================================================-->
        <!--jQuery [ REQUIRED ]-->
        <script src="<?php echo base_url(); ?>/assets/js/jquery-2.1.1.min.js"></script>
        <!--BootstrapJS [ RECOMMENDED ]-->
        <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
        <!--Fast Click [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>/assets/plugins/fast-click/fastclick.min.js"></script>
        <!--Jasmine Admin [ RECOMMENDED ]-->
        <script src="<?php echo base_url(); ?>/assets/js/scripts.js"></script>
        <!--Jquery Nano Scroller js [ REQUIRED ]-->
        <script src="<?php echo base_url(); ?>/assets/plugins/nanoscrollerjs/jquery.nanoscroller.min.js"></script>
        <!--Metismenu js [ REQUIRED ]-->
        <script src="<?php echo base_url(); ?>/assets/plugins/metismenu/metismenu.min.js"></script>
        <!--Switchery [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>/assets/plugins/switchery/switchery.min.js"></script>
        <!--Bootstrap Select [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
        <!--DataTables [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>/assets/plugins/datatables/media/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>/assets/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
        <!--Fullscreen jQuery [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>/assets/plugins/screenfull/screenfull.js"></script>
        <!--DataTables Sample [ SAMPLE ]-->
        <script src="<?php echo base_url(); ?>/assets/js/demo/tables-datatables.js"></script>
    </body>
</html>
