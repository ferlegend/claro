<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Lista de PDF</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Lista.</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/img/preview-Claro.ico">
    <?php
    require 'css.php'; ?>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #c41404;">
        <a class="navbar-brand" href="#" style="color: white">OC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto my-2 my-lg-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white">
                  Bienvenido <?php echo $_SESSION["username"];?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo base_url(); ?>index.php/welcome/salir">Logout</a>
                </div>
              </li>
          </ul>
        </div>
      </nav>
<br>
<br>
<br>
<br>
<br>

      <div class="row">
          <div class="col-md-3">

          </div>
          <div class="col-md-6">
            <h3 class="h3">Listado de Normativos</h3>
            <br>
  
            <a href="javascript:history.back()" class="btn btn-success"> Regresar</a>
            <br>
            <br>
            <table  class="table table-striped table-bordered">
                <thead>
                    <tr>
                         <th>No.</th>
                         <th>Titulo</th>
                         <th>Descripción</th>


                    </tr>
                </thead>

                <tbody>

                  <?php foreach ($usuarios as $usuario) {
                    // code...

                    ?>
                    <tr>
                       <td><?= $usuario->id_documento  ?></td>
                       <td>

                          <?php
                          $direccion ="";
                            //  echo $usuario->nombre_archivo."<br>";
                              if (preg_match("/.xlsx/", $usuario->nombre_archivo,$matches)){
                                 //print_r($matches[0]);

                                 if ($matches[0]=".xlsx") {
                                   //echo "es un excel";
                                   $direccion = "http://view.officeapps.live.com/op/view.aspx?src=ltechweb.org/tienda/".$usuario->nombre_archivo;
                                   // code...
                                 }

                              }else {
                                //echo "no excel";
                                $direccion =  base_url()."index.php/archivo/mostrar?id=".$usuario->id_documento;
                              }

                           ?>

                         <button  class="btn btn-link"><a  href="<?php echo $direccion; ?>"><?= $usuario->nombre_archivo ?></a></button>

                       </td>

                        <td><?= $usuario->descripcion  ?></td>


                    </tr>
                    <?php          } ?>

               </tbody>

            </table>
          </div>
          <div class="col-md-3">

          </div>
      </div>




    <!--===================================================-->
    <!-- END OF CONTAINER -->
    <!--JAVASCRIPT-->
    <!--=================================================-->
    <!--jQuery [ REQUIRED ]-->
    <script src="<?php echo base_url(); ?>/assets/js/jquery-2.1.1.min.js"></script>
    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
    <!--Fast Click [ OPTIONAL ]-->
    <script src="<?php echo base_url(); ?>/assets/plugins/fast-click/fastclick.min.js"></script>
    <!--Jasmine Admin [ RECOMMENDED ]-->
    <script src="<?php echo base_url(); ?>/assets/js/scripts.js"></script>
    <!--Jquery Nano Scroller js [ REQUIRED ]-->
    <script src="<?php echo base_url(); ?>/assets/plugins/nanoscrollerjs/jquery.nanoscroller.min.js"></script>
    <!--Metismenu js [ REQUIRED ]-->
    <script src="<?php echo base_url(); ?>/assets/plugins/metismenu/metismenu.min.js"></script>
    <!--Switchery [ OPTIONAL ]-->
    <script src="<?php echo base_url(); ?>/assets/plugins/switchery/switchery.min.js"></script>
    <!--Bootstrap Select [ OPTIONAL ]-->
    <script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <!--DataTables [ OPTIONAL ]-->
    <script src="<?php echo base_url(); ?>/assets/plugins/datatables/media/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>/assets/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <!--Fullscreen jQuery [ OPTIONAL ]-->
    <script src="<?php echo base_url(); ?>/assets/plugins/screenfull/screenfull.js"></script>
    <!--DataTables Sample [ SAMPLE ]-->
    <script src="<?php echo base_url(); ?>/assets/js/demo/tables-datatables.js"></script>
  </body>
</html>
