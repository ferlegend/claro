<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Principal</title>
      <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/img/preview-Claro.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php
      require 'css.php';
     ?>
     <style media="screen">

     </style>
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #c41404;">
        <a class="navbar-brand" href="#" style="color: white">OC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto my-2 my-lg-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white">
                  Bienvenido <?php echo $_SESSION["username"];?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo base_url(); ?>index.php/welcome/salir">Logout</a>
                </div>
              </li>



          </ul>


        </div>
      </nav>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="row">
      <div class="col-md-2">

      </div>
      <div class="col-md-4">
        <!--   navbar principal menu       -->
          <!-- https://storage.googleapis.com/nilh-pasionmovil.appspot.com/1/2018/06/claro-oficinas-cac.jpg -->
          <!--  navbar final -->
          <div class="card" style="">
            <img src="http://caliescribe.com/sites/default/files/derecho-administrativo.jpg" class="card-img-top" alt="..." height="300">
            <div class="card-body">
              <h5 class="card-title">Normativos</h5>
              <p class="card-text">En esta seccion puedes descargar manuales de los procesos.</p>
              <div class="btn-group btn-block btn-sm">
                  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Normativos
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Normativo 1</a>
                    <a class="dropdown-item" href="#">Normativo 2</a>
                    <a class="dropdown-item" href="#">Normativo 3</a>
                    <a class="dropdown-item" href="#">Normativo 4</a>
                  </div>
              </div>
            </div>
          </div>
      </div>
      <div class="col-md-4">
        <div class="card" style="">
          <img src="http://blog.atlanticdevices.com/FOTOSBLOG/S1C.jpg" class="card-img-top" alt="..." height="300">
          <div class="card-body">
            <h5 class="card-title">Sitios Claro GT</h5>
            <p class="card-text">Bienvenido: <?php $_SESSION["username"]; ?>  Usted puede acceder a todos los módulos del sistema.</p>

            <a href="<?php echo base_url(); ?>index.php/welcome/admin" class="btn btn-primary btn-block btn-sm">Entrar</a>

          </div>
        </div>
      </div>
      <div class="col-md-2">

      </div>
    </div>

<br>
<br>
<br>
<br>

    <footer class="footer">
      <nav class="navbar navbar-light" style="background-color: black;">
        <!-- Navbar content -->
        <a class="navbar-brand" href="#" style="color: white">Create: by AVP Solution.</a>
      </nav>
      </footer>
    <!-- Footer -->

  </body>
</html>
