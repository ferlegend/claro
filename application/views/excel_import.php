<!DOCTYPE html>
<html>
<head>
	<title>How to Import Excel Data into Mysql in Codeigniter</title>
<?php require 'css.php'; ?>
<script src="http://192.168.0.5:8888/claro/assets/jquery.min.js"></script>
</head>

<body>
	<div class="container">
		<br />
		<h3 align="center">Actualizacion de la base de datos</h3>
		<form method="GET" id="import_form" enctype="multipart/form-data" action="importar/import">
			<p><label>Seleccione el archivo de excel</label>
			<input type="file" name="file" id="file" required accept=".xls, .xlsx" /></p>
			<br />
			<input type="submit" name="import" value="Import" class="btn btn-info" />
		</form>
		<br />
		<div class="table-responsive" id="customer_data">

		</div>
	</div>
</body>
</html>

<script>
$(document).ready(function(){

	load_data();

	function load_data()
	{
		$.ajax({
			url:"<?php echo base_url(); ?>index.php/importar/fetch",
			method:"POST",
			success:function(data){
				$('#customer_data').html(data);
			}
		})
	}



});
</script>
