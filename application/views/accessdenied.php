<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Principal</title>
      <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/img/preview-Claro.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php
      require 'css.php';
     ?>
     <style media="screen">

     </style>
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #c41404;">
        <a class="navbar-brand" href="#" style="color: white">OC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto my-2 my-lg-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white">
                  Bienvenido <?php echo $_SESSION["username"];?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo base_url(); ?>index.php/welcome/salir">Logout</a>
                </div>
              </li>



          </ul>


        </div>
      </nav>
    <br>
    <br>
    <br>
    <br>
    <br>


      <div class="row">
          <div class="col-md-3">

          </div>
          <div class="col-md-6">
            <div class="card">
              <h5 class="card-header">Error</h5>
              <div class="card-body">
                <h5 class="card-title">Acceso Restringido: <?php echo $_SESSION["username"] ?></h5>
                <p class="card-text">usted no tiene acceso a este modulo.</p>
                <a href="http://192.168.0.5:8888/claro/index.php/welcome" class="btn btn-primary">Regresar</a>
              </div>
              </div>

          </div>
          <div class="col-md-3">

          </div>
      </div>





<br>
<br>
<br>
<br>

    <footer class="footer">
      <nav class="navbar navbar-light" style="background-color: black;">
        <!-- Navbar content -->
        <a class="navbar-brand" href="#" style="color: white">Create: by AVP Solution.</a>
      </nav>
      </footer>
    <!-- Footer -->

  </body>
</html>
