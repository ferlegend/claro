
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="~/js/RegisterForm.js">
        jQuery.validator.setDefaults({
            success: "valid"
        });
        $("#RegisterForm").validate({
            submitHandler: function(form) {
                $(form).ajaxSubmit();
            }
        })
    </script>
  </head>
  <body>
    <form id="RegisterForm" method="post">
        <div class="form-group">
            <label id="MobileNumber"></label>
            <input id="MobileNumber" name=”MobileNumber” placeholder="Mobile Number" class="form-control" />
        </div>
        <div class="form-group">
            <label id="Password"></label>
            <input id="Password" name=”Password” placeholder="Password" class="form-control" />
        </div>
        <div class="form-group">
            <label id="ConformPassword"></label>
            <input id="ConformPassword" name=”ConformPassword” placeholder="Conform Password" class="form-control" />
        </div>
        <div class="form-group">
            <label>
                <input id="TermCondition" name=”TermCondition” />I accept <a href="/term-and-condition">Terms and Conditions</a></label>
        </div>
        <button type="submit" class="btn btn-green ful-width">Submit <i class="far fa-edit"></i></button>
    </form>


  </body>
</html>
