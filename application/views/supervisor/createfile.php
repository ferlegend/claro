<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> solicitud de carpeta </title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/img/preview-Claro.ico">
        <!--STYLESHEET-->
        <!--=================================================-->
        <!--Roboto Slab Font [ OPTIONAL ] -->
        <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Roboto:500,400italic,100,700italic,300,700,500italic,400" rel="stylesheet">
        <!--Bootstrap Stylesheet [ REQUIRED ]-->


        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <!--Jasmine Stylesheet [ REQUIRED ]-->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <!--Font Awesome [ OPTIONAL ]-->
        <link href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!--Switchery [ OPTIONAL ]-->
        <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet">
        <!--Bootstrap Select [ OPTIONAL ]-->
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
        <!--ricksaw.js [ OPTIONAL ]-->
        <link href="<?php echo base_url(); ?>assets/plugins/jquery-ricksaw-chart/css/rickshaw.css" rel="stylesheet">
        <!--Bootstrap Validator [ OPTIONAL ]-->
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">
        <!--Demo [ DEMONSTRATION ]-->
        <link href="<?php echo base_url(); ?>assets/css/demo/jquery-steps.min.css" rel="stylesheet">
        <!--Summernote [ OPTIONAL ]-->
        <link href="<?php echo base_url(); ?>assets/plugins/summernote/summernote.min.css" rel="stylesheet">
        <!--Demo [ DEMONSTRATION ]-->
        <link href="<?php echo base_url(); ?>assets/css/demo/jasmine.css" rel="stylesheet">

        <link src="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/estilos.css" rel="stylesheet">
        <!--SCRIPT-->
        <!--=================================================-->
        <!--Page Load Progress Bar [ OPTIONAL ]-->
        <link href="<?php echo base_url(); ?>assets/plugins/pace/pace.min.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/plugins/pace/pace.min.js"></script>

        <script type="text/javascript">


        function validarEmail(elemento){

          var texto = document.getElementById(elemento.id).value;
          var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

          if (!regex.test(texto)) {
            document.getElementById("resultado").innerHTML = "Correo invalido";
          } else {
            document.getElementById("resultado").innerHTML = "";
          }





        }


        </script>

        <script type="text/javascript">


        function validarEmail2(elemento){

          var texto = document.getElementById(elemento.id).value;
          var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

          if (!regex.test(texto)) {
            document.getElementById("resultado2").innerHTML = "Correo invalido";
          } else {
            document.getElementById("resultado2").innerHTML = "";
          }



        }


        </script>


    </head>
    <!--TIPS-->
    <!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
    <body>
        <div id="container" class="effect mainnav-sm navbar-fixed mainnav-fixed">
            <!--NAVBAR-->
            <!--===================================================-->
            <header id="navbar">
                <div id="navbar-container" class="boxed">
                    <!--Navbar Dropdown-->
                    <!--================================-->
                    <div class="navbar-content clearfix">
                        <ul class="nav navbar-top-links pull-left">
                            <!--Navigation toogle button-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <li class="tgl-menu-btn">
                                <a class="mainnav-toggle" href="#"> <i class="fa fa-navicon fa-lg"></i> </a>
                            </li>

                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--End Navigation toogle button-->
                            <!--Messages Dropdown-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--End message dropdown-->
                            <!--Notification dropdown-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--End notifications dropdown-->
                        </ul>
                        <ul class="nav navbar-top-links pull-right">
                            <!--Profile toogle button-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <li class="hidden-xs" id="toggleFullscreen">
                                <a class="fa fa-expand" data-toggle="fullscreen" href="#" role="button">
                                <span class="sr-only">Toggle fullscreen</span>
                                </a>
                            </li>
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--End Profile toogle button-->
                            <!--User dropdown-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <li id="dropdown-user" class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">

                                    <div class="username hidden-xs"> Bienvenido: <?php echo $_SESSION["username"]; ?></div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right with-arrow">
                                    <!-- User dropdown menu -->
                                    <ul class="head-list">

                                        <li>
                                            <a href="<?php echo base_url(); ?>index.php/welcome/salir"> <i class="fa fa-sign-out fa-fw"></i> Salir </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--End user dropdown-->
                        </ul>
                    </div>
                    <!--================================-->
                    <!--End Navbar Dropdown-->
                </div>
            </header>
            <!--===================================================-->
            <!--END NAVBAR-->
            <div class="boxed">
                <!--CONTENT CONTAINER-->
                <!--===================================================-->
                <div id="content-container">
                <div class="pageheader hidden-xs">
                  <h3><i class="fa fa-envelope-o"></i> Solicitud de creacion de carpeta </h3>

                </div>
                    <!--Page content-->
                    <!--===================================================-->
                    <div id="page-content">
                        <div class="row">
                            <div class="col-md-12 eq-box-md grid">
                              <div class="panel">
                                  <div class="panel-heading">
                                      <div class="panel-control">

                                      </div>
                                      <h3 class="panel-title">Datos de envío</h3>
                                  </div>
                                  <!--No Label Form-->
                                  <!--===================================================-->
                                  <form class="form-horizontal"  name="formulario">
                                    <div class="panel-body">

                                        <div class="row">
                                          <div class="col-md-1 col-xs-12">
                                              <label for="" align="left">Para:</label>
                                          </div>
                                          <div class="col-md-6 col-xs-12 mar-btm">
                                              <input type="text" name="email" id="email" placeholder="ejemplo@gmail.com" class="form-control" maxlength="50" onkeyup="validarEmail(this)" required> <a id='resultado'></a>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-1 col-xs-12">
                                              <label for="" align="left">CC:</label>
                                          </div>
                                          <div class="col-md-6 col-xs-12 mar-btm">
                                              <input type="text" name="cc" id="cc" placeholder="ejemplo@gmail.com" class="form-control" maxlength="50" onkeyup="validarEmail2(this)" required> <a id='resultado2' ></a>
                                          </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1 col-xs-12">
                                                <label for="">Asunto:</label>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                 <input type="text" id="mensajemo" name="mensajemo" rows="9" class="form-control" placeholder="Descripción" maxlength="100" required>
                                            </div>
                                        </div>
                                    </div>
                                      <div class="panel-body">
                                        <!-- aqui va el foreach por cada carpeta a cargar -->
                                          <div class="row">
                                            <div class="col-md-2">
                                                <label for=""> ID</label><br>
                                                <input type="text" name="id" value="" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                              <label> Nombre del sitio</label><br>
                                              <input type="text" name="namesite" value="" class="form-control" placeholder="nombre del sitio">
                                            </div>
                                            <div class="col-md-2">
                                              <label for="">Latitud</label>
                                              <input type="text" name="latitud" value="" placeholder="234234" class="form-control">
                                            </div>
                                            <div class="col-md-2 mar-btm">
                                              <label for="">Longitud</label><br>
                                              <input type="text" name="longitud" value="" class="form-control" placeholder="-23433">
                                            </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-md-3 col-xs-12">
                                                <label for="">Mnemonico</label><br>
                                                <input type="text" name="mnemonico" value="" class="form-control" placeholder="" class="form-control">
                                              </div>
                                              <div class="col-md-3 col-xs-12">
                                                <label for="">Municipio</label> <br>
                                                <input type="text" name="municipio" value="" class="form-control" placeholder="municipio">
                                              </div>
                                              <div class="col-md-3 col-xs-12">
                                                  <label for="">Dirección</label>
                                                  <input type="text" name="direccion" value="" class="form-control" placeholder="direccion">
                                              </div>
                                              <div class="col-md-3 col-xs-12 mar-btm">
                                                  <label for="">Descripcion</label>
                                                  <input type="text" name="descripcion" value="" class="form-control" placeholder="descripcion">
                                              </div>

                                          </div>
                                          <div class="row">
                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                <label for="">Descripcion del arbol de carpeta</label>
                                                <input type="text" name="darbol" value="" class="form-control">
                                            </div>
                                          </div>
                                          <!-- finalizacion del foreach -->
                                          <!--<textarea placeholder="Message" rows="13" class="form-control"></textarea>-->

                                          <br>
                                          <br>
                                          <div class="row">
                                              <div class="col-md-2">

                                              </div>
                                            <div class="col-md-8 col-xs-12">
                                              <img src="<?php echo base_url(); ?>assets/img/construccion.jpg" class="img-fluid" alt="Responsive image">
                                            </div>
                                            <div class="col-md-2">

                                            </div>

                                          </div>



                                      </div>
                                      <div class="panel-footer text-right">
                                          <input value="Enviar" type="submit" class="btn btn-info" id="btnSend"  >
                                      </div>
                                  </form>
                                  <!--===================================================-->
                                  <!--End No Label Form-->
                              </div>
                            </div>

                        </div>


                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
                <!--MAIN NAVIGATION-->
                <!--===================================================-->
                <nav id="mainnav-container">
                    <!--Brand logo & name-->
                    <!--================================-->
                    <div class="navbar-header">
                        <a href="index.html" class="navbar-brand">
                          <i><img src="<?php echo base_url(); ?>/assets/img/SCL.png" width="60"> <font size="5" face="georgia">Obra Civil</font></i>

                        </a>
                    </div>
                    <!--================================-->
                    <!--End brand logo & name-->
                    <div id="mainnav">
                        <!--Menu-->
                        <!--================================-->
                        <div id="mainnav-menu-wrap">
                            <div class="nano">
                                <div class="nano-content">
                                    <ul id="mainnav-menu" class="list-group">
                                        <!--Category name-->
                                        <li class="list-header">Navegacion</li>
                                        <!--Menu list item-->
                                        <li> <a href="<?php echo base_url(); ?>index.php/welcome/supervisor"> <i class="fa fa-home"></i> <span class="menu-title"> Dashboard </span> </a> </li>
                                        <!--Category name-->
                                        <li class="list-header">Opciones</li>
                                        <li>
                                            <a href="#">
                                            <i class="fa fa-table"></i>
                                            <span class="menu-title">Principal</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="<?php echo base_url(); ?>index.php/welcome"><i class="fa fa-caret-right"></i>Pantalla Principal</a></li>

                                            </ul>
                                        </li>
                                        <!--Menu list item-->
                                        <li>
                                            <a href="#">
                                            <i class="fa fa-briefcase"></i>
                                            <span class="menu-title">Configuración</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a class="isDisabled"><i class="fa fa-caret-right"></i>Importar datos</a></li>
                                                <li><a class="isDisabled"><i class="fa fa-caret-right"></i>Usuarios</a></li>
                                            </ul>
                                        </li>

                                        <!--Menu list item-->
                                        <li>
                                            <a href="#">
                                            <i class="fa fa-table"></i>
                                            <span class="menu-title">Historial</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a class="isDisabled"><i class="fa fa-caret-right"></i>Solicitud de carpeta </a></li>

                                            </ul>
                                        </li>
                                        <!--Menu list item-->
                                        <li>
                                            <a href="#">
                                            <i class="fa fa-edit"></i>
                                            <span class="menu-title">Reportes</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="<?php echo base_url(); ?>index.php/report/arealegal"><i class="fa fa-caret-right"></i> Area Legal</a></li>
                                                <li><a href="<?php echo base_url(); ?>index.php/report/permisos_municipales"><i class="fa fa-caret-right"></i> Permisos M</a></li>
                                                <li><a href="forms-components.html"><i class="fa fa-caret-right"></i> Reporte 3 </a></li>

                                            </ul>
                                        </li>
                                        <!--Menu list item-->
                                        <li>
                                            <a href="#">
                                            <i class="fa fa-envelope-o"></i>
                                            <span class="menu-title">Solicitud</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="<?php echo base_url(); ?>index.php/createfile"><i class="fa fa-caret-right"></i>Creacion carpeta </a></li>

                                            </ul>
                                        </li>
                                        <li class="list-divider"></li>
                                        <!--Category name-->
                                        <li>
                                            <a href="#">
                                            <i class="fa fa-edit"></i>
                                            <span class="menu-title">Información general</span>
                                            <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="<?php echo base_url(); ?>index.php/welcome/"><i class="fa fa-caret-right"></i> Información</a></li>


                                            </ul>
                                        </li>
                                        <li>
                                          <a href="#">
                                          <i class="fa fa-briefcase"></i>
                                          <span class="menu-title">Indicadores</span>
                                          <i class="arrow"></i>
                                          </a>
                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="<?php echo base_url(); ?>index.php/indicadores/"><i class="fa fa-caret-right"></i>Información</a></li>


                                            </ul>
                                        </li>




                                    </ul>
                                    <!--Widget-->
                                    <!--================================-->

                                    <!--================================-->
                                    <!--End widget-->
                                </div>
                            </div>
                        </div>
                        <!--================================-->
                        <!--End menu-->
                    </div>
                </nav>
                <!--===================================================-->
                <!--END MAIN NAVIGATION-->
            </div>
            <!-- FOOTER -->
            <!--===================================================-->
            <footer id="footer">
                <!-- Visible when footer positions are fixed -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="show-fixed pull-right">
                    <ul class="footer-list list-inline">
                        <li>
                            <p class="text-sm">SEO Proggres</p>
                            <div class="progress progress-sm progress-light-base">
                                <div style="width: 80%" class="progress-bar progress-bar-danger"></div>
                            </div>
                        </li>
                        <li>
                            <p class="text-sm">Online Tutorial</p>
                            <div class="progress progress-sm progress-light-base">
                                <div style="width: 80%" class="progress-bar progress-bar-primary"></div>
                            </div>
                        </li>
                        <li>
                            <button class="btn btn-sm btn-dark btn-active-success">Checkout</button>
                        </li>
                    </ul>
                </div>
                <!-- Visible when footer positions are static -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <div class="hide-fixed pull-right pad-rgt">Version 1.0</div>
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
                <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                <p class="pad-lft">&#0169; 2019 AVP Solution & support</p>
            </footer>
            <!--===================================================-->
            <!-- END FOOTER -->
            <!-- SCROLL TOP BUTTON -->
            <!--===================================================-->
            <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
            <!--===================================================-->
        </div>
        <!--===================================================-->
        <!-- END OF CONTAINER -->
        <!--JAVASCRIPT-->
        <!--=================================================-->
        <!--jQuery [ REQUIRED ]-->


        <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validar.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
        <!--BootstrapJS [ RECOMMENDED ]-->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <!--Fast Click [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/fast-click/fastclick.min.js"></script>
        <!--Jquery Nano Scroller js [ REQUIRED ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/nanoscrollerjs/jquery.nanoscroller.min.js"></script>
        <!--Metismenu js [ REQUIRED ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/metismenu/metismenu.min.js"></script>
        <!--Jasmine Admin [ RECOMMENDED ]-->
        <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
        <!--Switchery [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>
        <!--Jquery Steps [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/parsley/parsley.min.js"></script>
        <!--Jquery Steps [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/jquery-steps/jquery-steps.min.js"></script>
        <!--Bootstrap Select [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
        <!--Bootstrap Wizard [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
        <!--Masked Input [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/masked-input/bootstrap-inputmask.min.js"></script>
        <!--Bootstrap Validator [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-validator/bootstrapValidator.min.js"></script>
        <!--Flot Chart [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/flot-charts/jquery.flot.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/flot-charts/jquery.flot.resize.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/flot-charts/jquery.flot.spline.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/flot-charts/jquery.flot.pie.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/moment/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/moment-range/moment-range.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/flot-charts/jquery.flot.tooltip.min.js"></script>
        <!--Flot Order Bars Chart [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/flot-charts/jquery.flot.categories.js"></script>
        <!--ricksaw.js [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/jquery-ricksaw-chart/js/raphael-min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery-ricksaw-chart/js/d3.v2.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery-ricksaw-chart/ricksaw.js"></script>
       <!--Summernote [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/summernote/summernote.min.js"></script>
        <!--Fullscreen jQuery [ OPTIONAL ]-->
        <script src="<?php echo base_url(); ?>assets/plugins/screenfull/screenfull.js"></script>
        <!--Form Wizard [ SAMPLE ]-->
        <script src="<?php echo base_url(); ?>assets/js/demo/wizard.js"></script>
        <!--Form Wizard [ SAMPLE ]-->
        <script src="<?php echo base_url(); ?>assets/js/demo/form-wizard.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/demo/dashboard-v2.js"></script>

    </body>
</html>
