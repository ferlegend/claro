<?php
class Model_Historial extends CI_Model
{

  public function listado_solicitudes(){

    $this->load->database();
    $query = $this->db->query("
    select
      cl_histo_id,
      cl_histo_destinatario,
      cl_histo_cc,
      cl_histo_asunto,
      cl_histo_usuario,
      cl_histo_fecha
    from
      cl_historial
      ");
    return $query->result();

  }

  public function totalsolicitudes(){
      $this->load->database();
      $query = $this->db->query('
        select count(cl_histo_id) as cantidad from cl_historial
      ');
      return $query->result();
  }
  public function total_solicitudes_por_usuario(){
    $this->load->database();
    $query=$this->db->query('
    select cl_histo_usuario, count(cl_histo_usuario) as numero_solicitudes
    from cl_historial
    group by cl_histo_usuario
    having count(cl_histo_usuario)>0
    ');
    return $query->result();
  }

}

 ?>
