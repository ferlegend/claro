<?php

class Model_Sitios extends CI_Model
{

  public function listado(){

    $this->load->database();
    $query = $this->db->query("
    select node_b_name, site_name, latitud, longitud, mnemonico, municipio, direccion, tipologia
    from cl_basegeneral
      ");

    return $query->result();


  }

  public function buscar($codigo){
    $this->load->database();
    $query = $this->db->query("
    select node_b_name, site_name, latitud, longitud, mnemonico, municipio, direccion, tipologia
    from cl_basegeneral
    where node_b_name ='".$codigo."'
      ");
    return $query->result();

  }


  public function guardar($operador,$idsitio,$nombresi,$idclaro,$departamento,$municipio,$ingcelltype,$altura,$latitud,$longitud,$medio,
                          $estructurasitio,$infra,$tipologia,$tarrendadorcom,$trenta,$tvigencia,$tperiodo,$tarrecom,$trenmen,$tvigcon,
                          $torreropeforz,$coubicacion,$condicionesoperativastorre,$restriccionesdeacceso,$licenciasvigen,$alturadeto,
                          $marcdetorre,$velocitorr,$antiguetorr,$tipoinfra,$ultmantetorr,$planostorr,$planossit,$comercial,$tipodener,
                          $marn,$dgac,$licenciaconst,$cocode,$otrosapor,$problemas,$penalizaciones,$sobrecargaor,$radioenlace,$mnemonico,
                          $direccion){

    $this->load->database();
    $query = $this->db->query("
    insert into cl_basegeneral(
     operador,
     node_b_name,
     site_name,
     id_claro,
     departamento,
     municipio,
     ing_cell_type,
     altura_derf,
     latitud,
     longitud,
     medio_de_tx,
     estructura_de_sitio,
     infra,
     tipologia,
     terreno_arrendador_compras,
     terreno_renta_mensual,
     terreno_vigencia_contrato,
     terreno_periodo_forzoso,
     torrero_arrendador_compras,
     torrero_renta_mensual_compras,
     torrero_vigencia_contrato_compras,
     torrero_periodo_forzoso,
     coubicacion_de_terceros,
     condiciones_operativas_torre,
     restricciones_de_acceso,
     licencias_vigentes,
     altura_de_torre,
     marca_de_torre,
     velocidad_torre,
     antiguedad_torre,
     tipo_infraestructura,
     ultimo_mantenimiento_torre,
     planos_torre, planos_sitio,
     e_comercial_e_continua,
     tipo_de_energia,
     marn,
     dgac,
     licencia_construccion,
     cocode,
     otros_aportes,
     problemas,
     penalizaciones,
     t_sobrecarga_o_riesgo,
     c_e_radioenlace,
     mnemonico,
     direccion
     )values(
       '".$operador."',
       '".$idsitio."',
       '".$nombresi."',
       '".$idclaro."',
       '".$departamento."',
       '".$municipio."',
       '".$ingcelltype."',
       '".$altura."',
       '".$latitud."',
       '".$longitud."',
       '".$medio."',
       '".$estructurasitio."',
       '".$infra."',
       '".$tipologia."',
       '".$tarrendadorcom."',
       '".$trenta."',
       '".$tvigencia."',
       '".$tperiodo."',
       '".$tarrecom."',
       '".$trenmen."',
       '".$tvigcon."',
       '".$torreropeforz."',
       '".$coubicacion."',
       '".$condicionesoperativastorre."',
       '".$restriccionesdeacceso."',
       '".$licenciasvigen."',
       '".$alturadeto."',
       '".$marcdetorre."',
       '".$velocitorr."',
       '".$antiguetorr."',
       '".$tipoinfra."',
       '".$ultmantetorr."',
       '".$planostorr."',
       '".$planossit."',
       '".$comercial."',
       '".$tipodener."',
       '".$marn."',
       '".$dgac."',
       '".$licenciaconst."',
       '".$cocode."',
       '".$otrosapor."',
       '".$problemas."',
       '".$penalizaciones."',
       '".$sobrecargaor."',
       '".$radioenlace."',
       '".$mnemonico."',
       '".$direccion."')
    ");

  }


}



 ?>
