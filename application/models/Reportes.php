<?php
class Reportes extends CI_Model
{

  public function area_legal(){

    $this->load->database();
    $query = $this->db->query("
    select
      id_claro,
      site_name,
      departamento,
      municipio,
      latitud,
      longitud,
      ing_cell_type,
      estructura_de_sitio,
      licencia_construccion
    from cl_basegeneral

      ");
    return $query->result();

  }
  public function area_legal_permisos_municipales(){
    $this->load->database();
    $query = $this->db->query("
    select
      id_claro,
      site_name,
      departamento,
      municipio,
      latitud,
      longitud,
      ing_cell_type,
      estructura_de_sitio
    from cl_basegeneral

      ");
    return $query->result();

  }

}


 ?>
