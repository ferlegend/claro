<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {

    function insertRecord($record){
      $this->load->database();

        if(count($record) > 0){

            // Check user

            $this->db->select('*');
            $this->db->where('id_claro', $record[4]);
            $q = $this->db->get('cl_basegeneral');
            $response = $q->result_array();

            // Insert record
           if(count($response) == 0){
                $newuser = array(
                    "operador" => trim($record[0]),
                    "node_b_name" => trim($record[1]),
                    "site_name" => trim($record[2]),
                    "id_claro" => trim($record[3]),
                    "departamento" => trim($record[4]),
                    "municipio" => trim($record[5]),
                    "ing_cell_type" => trim($record[6]),
                    "altura_derf" => trim($record[7]),
                    "latitud" => trim($record[8]),
                    "longitud" => trim($record[9]),
                    "medio_de_tx" => trim($record[10]),
                    "estructura_de_sitio" => trim($record[11]),
                    "infra" => trim($record[12]),
                    "tipologia" => trim($record[13]),
                    "terreno_arrendador_compras" => trim($record[14]),
                    "terreno_renta_mensual" => trim($record[15]),
                    "terreno_vigencia_contrato" => trim($record[16]),
                    "terreno_periodo_forzoso" => trim($record[17]),
                    "torrero_arrendador_compras" => trim($record[18]),
                    "torrero_renta_mensual_compras" => trim($record[19]),
                    "torrero_vigencia_contrato_compras" => trim($record[20]),
                    "torrero_periodo_forzoso" => trim($record[21]),
                    "coubicacion_de_terceros" => trim($record[22]),
                    "condiciones_operativas_torre" => trim($record[23]),
                    "restricciones_de_acceso" => trim($record[24]),
                    "licencias_vigentes" => trim($record[25]),
                    "altura_de_torre" => trim($record[26]),
                    "marca_de_torre" => trim($record[27]),
                    "velocidad_torre" => trim($record[28]),
                    "antiguedad_torre" => trim($record[29]),
                    "tipo_infraestructura" => trim($record[30]),
                    "ultimo_mantenimiento_torre" => trim($record[31]),
                    "planos_torre" => trim($record[32]),
                    "planos_sitio" => trim($record[33]),
                    "e_comercial_e_continua" => trim($record[34]),
                    "tipo_de_energia" => trim($record[35]),
                    "marn" => trim($record[36]),
                    "dgac" => trim($record[37]),
                    "licencia_construccion" => trim($record[38]),
                    "cocode" => trim($record[39]),
                    "otros_aportes" => trim($record[40]),
                    "problemas" => trim($record[41]),
                    "penalizaciones" => trim($record[42]),
                    "t_sobrecarga_o_riesgo" => trim($record[43]),
                    "c_e_radioenlace" => trim($record[44])
                );

                $this->db->insert('cl_basegeneral', $newuser);
           }

        }

    }

}
