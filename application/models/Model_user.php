<?php
class Model_User extends CI_Model
{

  public function user(){

    $this->load->database();
    $query = $this->db->query("
    select usu.cl_usu_id, usu.cl_usu_nombre, usu.cl_usu_usuario,usu.cl_usu_departamento, tipo.cl_tipo_descripcion,usu.cl_usu_correo
    from cl_usuarios as usu inner join cl_tipo_usuario tipo
    where usu.cl_tipo_usuario = tipo.cl_tipo_usu_id
      ");
    return $query->result();

  }
  public function delete($id){
      $this->load->database();
      $query = $this->db->query("
      delete from cl_usuarios
      where cl_usu_id ='".$id."'
      ");
    //  return $query->result();
  }
  public function guardar($nombre,$usuario,$password,$ip,$fecha,$correo,$tipo){
      $this->load->database();
      $query=$this->db->query("
      insert into cl_usuarios(
      cl_usu_nombre,
      cl_usu_usuario,
      cl_usu_password,
      cl_usu_departamento,
      cl_usu_ip,
      cl_usu_correo,
      cl_usu_fecha,
      cl_tipo_usuario
      )values(
      '".$nombre."',
      '".$usuario."',
      '".$password."',
      'obra civil',
      '".$ip."',
      '".$correo."',
      STR_TO_DATE('".$fecha."', '%d-%m-%Y'),
      '".$tipo."')
      ");
  }
  public function actualizar($nombre,$usuario,$id,$tipo,$nuevocorreo){
    $this->load->database();
    $query= $this->db->query("
    update cl_usuarios
      set
      cl_usu_nombre='".$nombre."',
        cl_usu_usuario='".$usuario."',
        cl_tipo_usuario ='".$tipo."',
        cl_usu_correo ='".$nuevocorreo."'
      where cl_usu_id = '".$id."'

    ");
  }
    public function validar_correo($correo){
        $this->load->database();
        $query = $this->db->query("
        select  cl_usu_nombre, cl_usu_correo
        from cl_usuarios
        where cl_usu_correo = '".$correo."'
        ");
        return $query->result();
    }
    public function  password_update($pass,$nombre,$correo){
        $this->load->database();
        $query = $this->db->query("
        update cl_usuarios
          set
            cl_usu_password = '".$pass."'
          where
            cl_usu_nombre ='".$nombre."'
          and
            cl_usu_correo='".$correo."'
        ");

    }
    public function tipo_de_usuarios(){

      $this->load->database();
      $query = $this->db->query('
        select * from cl_tipo_usuario

      ');
      return $query->result();
    }
}

 ?>
