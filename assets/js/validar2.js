$(document).ready(function(){

    $('#btnSend').click(function(){

        var errores = '';

        // Validado Nombre ==============================
        if( $('#nombreusuario').val() == '' ){
            errores += '<p>Ingrese un nombre</p>';
            $('#nombreusuario').css("border-bottom-color", "#F14B4B")
        } else{
            $('#nombreusuario').css("border-bottom-color", "#d1d1d1")
        }

        // Validado Correo ==============================
        if( $('#apellidousuario').val() == '' ){
            errores += '<p>Ingrese un apellido</p>';
            $('#apellidousuario').css("border-bottom-color", "#F14B4B")
        } else{
            $('#apellidousuario').css("border-bottom-color", "#d1d1d1")
        }

        if( $('#username').val() == '' ){
            errores += '<p>Escriba un usuario</p>';
            $('#username').css("border-bottom-color", "#F14B4B")
        } else{
            $('#username').css("border-bottom-color", "#d1d1d1")
        }

        if( $('#password').val() == '' ){
            errores += '<p>Escriba una contraseña</p>';
            $('#password').css("border-bottom-color", "#F14B4B")
        } else{
            $('#password').css("border-bottom-color", "#d1d1d1")
        }
        // Validado Mensaje ==============================
        if( $('#email').val() == '' ){
            errores += '<p>Escriba un correo</p>';
            $('#email').css("border-bottom-color", "#F14B4B")
        } else{
            $('#email').css("border-bottom-color", "#d1d1d1")
        }

        if( $('#cofirmarusername').val() == '' ){
            errores += '<p>No ha confirmado su usuario</p>';
            $('#cofirmarusername').css("border-bottom-color", "#F14B4B")
        } else{
            $('#cofirmarusername').css("border-bottom-color", "#d1d1d1")
        }

        if( $('#confirmarpassword').val() == '' ){
            errores += '<p>No ha confirmado su contraseña</p>';
            $('#confirmarpassword').css("border-bottom-color", "#F14B4B")
        } else{
            $('#confirmarpassword').css("border-bottom-color", "#d1d1d1")
        }


        // ENVIANDO MENSAJE ============================
        if( errores == '' == false){
            var mensajeModal = '<div class="modal_wrap">'+
                                    '<div class="mensaje_modal">'+
                                        '<h3>Errores encontrados</h3>'+
                                        errores+
                                        '<span id="btnClose">Cerrar</span>'+
                                    '</div>'+
                                '</div>'

            $('body').append(mensajeModal);
        }

        // CERRANDO MODAL ==============================
        $('#btnClose').click(function(){
            $('.modal_wrap').remove();
        });
    });

});
